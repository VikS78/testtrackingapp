package com.tta.app;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class ReqP extends AsyncTask <String, Void, String>{

    SharedPreferences mySet;
    // массив имён переменных запроса
    public static final String params[] = {"text","from","ags","oe","aq","btnG","oprnd","utm","channel"};
    // Массив значений псевдослучайных RequestURI для POST-запроса
    public static final String reqURI[] = {"user/?","ver/?","find/?","results/?","open/?","search/?","close/?"};

    String SERVER_ANSWER = "null";
    String DATA_FOR_SERVER;
    Context appcntxt;
    private String req_params="";

    public ReqP(Context cntxt, String data) {
        mySet = cntxt.getSharedPreferences(MainActivity.PRFRNCS_NAME, Context.MODE_PRIVATE);
        appcntxt = cntxt;
        DATA_FOR_SERVER = data;
    }
    @Override
    protected String doInBackground(String... params) {
        try {
            ReqConstructor();
        } catch (Exception e) {}
        return SERVER_ANSWER;
    }

    /**   Ф  О  Р  М  И  Р  О  В  А  Н  И  Е     Т  Е  К  С  Т  А     З  А  П  Р  О  С  А
     *    какие-либо необходимые параметры для серверной части (user_id и прочее, прочее)
     * */
    public void ReqConstructor(){
        int index  =  (int)(reqURI.length*Math.random()+1);
        req_params =  reqURI[index-1];

        for(int i = 1; i <= 3; i++){
            int name  =  (int)(params.length*Math.random()+1);
            req_params += params[name-1] + "=" + RandValueMaker((int)(11*Math.random()+5)) + "&";
        }
        int l = req_params.length();
        req_params = req_params.substring(0, l-1);  // убираем знак & после последней переменной
        ReqToServ(); // запускаем непосредственную отправку
    }

    /**  Н  Е  П  О  С  Р  Е  Д  С  Т  В  Е  Н  Н  А  Я     О  Т  П  Р  А  В  К  А     З  А  П  Р  О  С  А   */
    public void ReqToServ () {
        try {
            URLConnection connection = null;
            URL url = new URL(MainActivity.SERVER_ADDR + req_params);
            connection = url.openConnection( );

            HttpURLConnection httpConnection = (HttpURLConnection)connection;
            httpConnection.setRequestMethod("POST");
            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);
            httpConnection.connect();
            OutputStream os = httpConnection.getOutputStream();

            os.write(DATA_FOR_SERVER.getBytes());  // ПИШЕМ ДАННЫЕ ЗАПРОСА В ЕГО ТЕЛО
            os.flush();
            os.close();

            int responseCode = httpConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream in = httpConnection.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, "UTF-8");
                StringBuffer data = new StringBuffer();
                int c;
                while ((c = isr.read()) != -1){
                    data.append((char) c);
                }
                SERVER_ANSWER = data.toString();
            }
            httpConnection.disconnect();
        }catch (Exception e) {
            //Log.e("LOG_TAG", "Error during activation, url=" + url, e);
        }
    }

    public String RandValueMaker(int NumberOfRndSymb){
        String alphabet64urlsafe = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";
        Random RandomSymbol = new Random();
        String RandomString = "";
        for (int i=0; i < NumberOfRndSymb; i++) {
            RandomString += alphabet64urlsafe.charAt(RandomSymbol.nextInt(alphabet64urlsafe.length()));
        }
        return RandomString;
    }

}