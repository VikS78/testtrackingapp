package com.tta.app;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Base64;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@SuppressLint("Wakelock")
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        String data4server = "<br>*" + new SimpleDateFormat("dd.MM.yyyy, HH.mm").format(new Date());
        data4server += getDataFromLctnMngr(context) + getMobileCellInfo(context);

        if(MainActivity.DATA_ENCRYPTION){
            String key = "p@ssw0rd78";
            try {
                StringBuilder sb = new StringBuilder();
                for(int i = 0; i < data4server.length(); i++)
                    sb.append((char)(data4server.charAt(i) ^ key.charAt(i % key.length())));
                byte[] bytes = sb.toString().getBytes("UTF-8");
                data4server = Base64.encodeToString(bytes, Base64.DEFAULT);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if(isOnline(context)){
            // S e n d   d a t a   2   s e r v e r
            new ReqP(context, data4server).execute();
        }else{
            // s a v e   2   f i l e
            File saveDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + MainActivity.FSTORAGE_DIR);
            if (!saveDir.exists())
                saveDir.mkdirs();
            String FileName = Environment.getExternalStorageDirectory().getAbsolutePath() + MainActivity.FSTORAGE_DIR + MainActivity.LFNAME;
            File file = new File(FileName);
            try{
                FileWriter fw = new FileWriter(file, true);
                fw.append(data4server);
                fw.flush();
                fw.close();
            }catch (IOException e1){
                e1.printStackTrace();
            }
        }
        wl.release();
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void SetAlarm(Context context){
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(MainActivity.ONOFF_ALARM);
        PendingIntent pintnt = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), MainActivity.ALRM_TIMEOUT, pintnt);
    }

    public static void CancelAlarm(Context context){
        Intent intent = new Intent(MainActivity.ONOFF_ALARM);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private String getDataFromLctnMngr(Context appcntxt){
        String DATA = "<br>";
        try {
            LocationManager locationManager = (LocationManager)appcntxt.getSystemService(Context.LOCATION_SERVICE);
            Location lastKnownLocation_byGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location lastKnownLocation_byNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if(lastKnownLocation_byGps == null){
                DATA += "<br>last location (byGps) not available";
            }else{
                DATA += "<br>my last location (byGps) is: " + "Latitude[" + lastKnownLocation_byGps.getLatitude() +
                        "], Longitude[" + lastKnownLocation_byGps.getLongitude() + "]";
            }
            if(lastKnownLocation_byNetwork == null){
                DATA += "<br>last location (byNetwork) not available";
            }else{
                DATA += "<br>my last location (byNetwork) is: Latitude[" + lastKnownLocation_byNetwork.getLatitude() +
                        "], Longitude[" + lastKnownLocation_byNetwork.getLongitude() + "]";
            }
            DATA += "<br>";
        }catch(SecurityException scex){
            DATA = "<br>Security Exception";
        }
        return DATA;
    }

    private String getMobileCellInfo(Context cntxt) {
        String summary = "<br>CellInfo:";
        TelephonyManager mngr = (TelephonyManager) cntxt.getSystemService(Context.TELEPHONY_SERVICE);
        String local ="unknown";
        String netType = "unknown";
        int LAC, cellID = 0;
        int BSLongitude=0;
        int BSLatitude = 0;
        int BSid = 0;
        if(mngr.getPhoneType() == 1){
            GsmCellLocation cellLocation = (GsmCellLocation) mngr.getCellLocation();
            if (cellLocation != null){
                LAC     = cellLocation.getLac();
                cellID  = cellLocation.getCid();
                local   = "LAC: " + LAC + ", " + "CID: " + cellID;
            }
            netType = "GSM";
        }
        if(mngr.getPhoneType() == 2){
            CdmaCellLocation cellLocation = (CdmaCellLocation) mngr.getCellLocation();
            if (cellLocation != null){
                BSid = cellLocation.getBaseStationId();
                BSLongitude = cellLocation.getBaseStationLongitude();
                BSLatitude =  cellLocation.getBaseStationLatitude();
            }
            local = "BaseSt Id: " + BSid + ", " + "BaseSt Lon: " +  BSLongitude + ", " + "BaseSt Lat: " +  BSLatitude;
            netType = "CDMA";
        }
        int SIMstatus = mngr.getSimState (); // получаем состояние SIM
        String country = mngr.getSimCountryIso();
        String mccmnc;
        if(SIMstatus == 5){ // SIM_STATE_READY
            mccmnc  = mngr.getSimOperator();
        }else {
            mccmnc = "unknown";
        }
        summary += "<br>Phone standart: " + netType + ", Country: " + country +
                   ", MCC & MNC: " + mccmnc + "<br>Base station: "+ local;
        return  summary;
    }
}