package com.tta.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // C O N S T A N T S
    public static final String APP_PACKAGE = "com.tta.app";
    public static final String FSTORAGE_DIR = "/Android/data/" + APP_PACKAGE + "/";
    public static final String ONOFF_ALARM = APP_PACKAGE + ".START_ALARM";
    public static final String LFNAME = "fldata.txt";

    public static final String SERVER_ADDR = "http://127.0.0.1/";
    public static final Boolean DATA_ENCRYPTION = false;    // U S E   E N C R Y P T I O N   T R U E   F A L S E
    public static final int ALRM_TIMEOUT = 3000 * 60;       // 3    M I N U T E S

    public static final String PRFRNCS_NAME = "prefs";
    public static final String ALARM_IS_ACTIVATED = "alrm";

    SwitchCompat swtch1;
    TextView onoffTv;
    SharedPreferences mShrdPrfs;
    String MSSG_ON = "monitor is ON", MSSG_OFF = "monitor is OFF";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;

        mShrdPrfs = getSharedPreferences(PRFRNCS_NAME, Context.MODE_PRIVATE);
        Boolean isActivated = mShrdPrfs.getBoolean(ALARM_IS_ACTIVATED, false);

        swtch1 = (SwitchCompat)findViewById(R.id.switch_mode1);
        onoffTv = (TextView)findViewById(R.id.onoff_text);
        swtch1.setChecked(isActivated);
        onoffTv.setText(isActivated ? MSSG_ON : MSSG_OFF);
        onoffTv.setTextColor(isActivated ? ContextCompat.getColor(context, R.color.ColorPrimaryDark) : ContextCompat.getColor(context, R.color.DarkRed));

        swtch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onoffTv.setText(MSSG_ON);
                    onoffTv.setTextColor(ContextCompat.getColor(context, R.color.ColorPrimaryDark));
                    AlarmReceiver.SetAlarm(context);
                } else {
                    onoffTv.setText(MSSG_OFF);
                    onoffTv.setTextColor(ContextCompat.getColor(context, R.color.DarkRed));
                    AlarmReceiver.CancelAlarm(context);
                }
                SharedPreferences.Editor editor = mShrdPrfs.edit();
                editor.putBoolean(ALARM_IS_ACTIVATED, isChecked);
                editor.apply();
            }
        });

        //  4   M A R S H M A L L O W
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Toast.makeText(context, "o_0", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
